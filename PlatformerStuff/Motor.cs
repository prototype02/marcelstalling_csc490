using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class Motor : MonoBehaviour {
	
	public float speed = 1f;
	public float maxSpeedChange = 1f;
	public float jumpHeight = 1f;
	
	[HideInInspector]
	public bool shouldJump = false;
	
	[HideInInspector]
	public Vector3 targetVelocity = Vector3.zero;
	
	bool grounded = false;
	Vector3 contactNormal = Vector3.zero;
	
	public bool InAir {
		get {
			return !grounded;
		}
	}
	
	public bool OnGround {
		get {
			return grounded;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate() {
		Vector3 velocity = rigidbody.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, 0, maxSpeedChange);
		velocityChange.y = 0;
		
		if (shouldJump && OnGround) {
			rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
		}
		
		rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
		
		grounded = false;
	}
	
	void OnCollisionStay(Collision collision) {
		foreach (ContactPoint cp in collision.contacts) {
			contactNormal = cp.normal;
			//float norm = Mathf.Rad2Deg * Mathf.Atan2(contactNormal.y, Mathf.Abs(contactNormal.x));
			if (contactNormal.y > Mathf.Abs(contactNormal.x))
				grounded = true;
		}
	}
	
	float CalculateJumpVerticalSpeed() {
		float g = Physics.gravity.y;
		return Mathf.Sqrt(2 * jumpHeight * -g);
	}
}
