using UnityEngine;
using System.Collections;
//using Glenn King's code in hopes of having a ball behave the way it should
public class gBall : MonoBehaviour 
{
        //[HideInInspector]
        public float minspeed = .25f;
        
        
        public bool inUse = false;
        
        private int life = 3;
        private int score = 0;
        private int highScore = 0;
        
        Vector3 v;
        
        int count = 15;
        
        
        float followX = 0;
        float followY = 0;
        float followZ = 0;
        
        public TextMesh lifeCountTextMesh;
        public TextMesh scoreCountTextMesh;
        public TextMesh highScoreCountTextMesh;
        
        
        // Use this for initialization
        void Start () 
        {       
        }
        
        // Update is called once per frame
        void Update () 
        {
                if(score > PlayerPrefs.GetInt("highScore"))
                        PlayerPrefs.SetInt("highScore",score);

                
                v = rigidbody.velocity; 
                Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
                Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
                
                if(!Input.GetButton("Jump") && inUse == false)
                {
                        
                        followX = GameObject.FindGameObjectWithTag("Player").transform.position.x;
                        followY = GameObject.FindGameObjectWithTag("Player").transform.position.y;
                        followZ = GameObject.FindGameObjectWithTag("Player").transform.position.z;
                
                        rigidbody.transform.position = new Vector3(followX, (followY + 1f) , followZ);
                        
                }
                
                if(Input.GetButton("Jump"))
                        inUse = true;
                
            if(inUse)
                {
                        inUse = true;
                        
                        if(count > 10)
                                rigidbody.AddForce(Vector3.up * minspeed);
                        
                        if (v.magnitude < minspeed || v.magnitude > minspeed)
                        {
                                v.Normalize();
                                v *= minspeed;
                                rigidbody.velocity = v;
                        }
                
                        if (topRight.x > 1) 
                                rigidbody.velocity = new Vector3(-Mathf.Abs(v.x), v.y, v.z);
                
                        else if (bottomLeft.x < 0)
                                rigidbody.velocity = new Vector3(Mathf.Abs(v.x), v.y, v.z);                     
                
                
                        if (topRight.y > 1) 
                                rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
                
                        else if (bottomLeft.y < 0)
                        {
                                followX = GameObject.FindGameObjectWithTag("Player").transform.position.x;
                                followY = GameObject.FindGameObjectWithTag("Player").transform.position.y;
                                followZ = GameObject.FindGameObjectWithTag("Player").transform.position.z;
                                
                                rigidbody.transform.position = new Vector3(followX, (followY + 2f) , followZ);
                                
                                life--;
                                lifeCountTextMesh.text = life.ToString();
                                //count = 16;
                                inUse = false;
                        }
                        
                        count--;
                        
                        if(life == 0)
                                Application.LoadLevel(0);
                }       
        }
        
        
        void OnCollisionEnter()
        {
                
                if(this.collider && GameObject.FindGameObjectWithTag("Player").collider)
                {
                    float ballDist = 0;
                    //float percentDist;
                        
                        if(v.x < 0)
                        {
                                ballDist = this.transform.position.x - GameObject.FindGameObjectWithTag("Player").transform.position.x;
                                v.x += ballDist * 100;                          
                                this.rigidbody.AddForce(new Vector3(v.x , v.y, v.z));
                        }
                        
                        else if(v.x > 0)
                        {
                                ballDist = GameObject.FindGameObjectWithTag("Player").transform.position.x - this.transform.position.x;
                                v.x += ballDist * 100;  
                                this.rigidbody.AddForce(new Vector3(v.x , v.y, v.z));
                        }       
                }       
                
                if(collider && GameObject.FindGameObjectWithTag("brickSimple"))
                {
                        score++;
                        scoreCountTextMesh.text = score.ToString();
                }
                
                if(highScore < score)
                        highScore = score;
                
                highScoreCountTextMesh.text = highScore.ToString();
                 
    }
        
}