using UnityEngine;
using System.Collections;
//public TextMesh someWords;


public class Ball : MonoBehaviour 
{
	public TextMesh scoreCount;
	public TextMesh lifeCount;
	public int score;
	public int life = 3;
	public float speed = 1;
	// Use this for initialization
	void Start () 
	{
		rigidbody.velocity = Vector3.one *speed;
	}
	
	
	
	
	
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 v = rigidbody.velocity;
		Vector3 posScreenTR = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 posScreenBL = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		if (posScreenTR.y > 1 )
		{
			rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
		}
		
		else if (posScreenBL.y < 0)
		{
			
			//gonna have to do something here, loose a life or somen'
			life--;
			lifeCount.text = life.ToString ();
			
			transform.position = Vector3.zero;
		}
		
		if (posScreenTR.x > 1)
		{
			rigidbody.velocity = new Vector3(-Mathf.Abs(v.x), v.y, v.z);
		}
		else if (posScreenBL.x < 0)
		{
			rigidbody.velocity = new Vector3(Mathf.Abs(v.x), v.y, v.z);
		}
		
	}
	
}
